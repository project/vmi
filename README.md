# View Modes Inventory

 This module has a set of template view modes that we typically use
 (some of them) in each website.

## View Modes Inventory Layouts peered with the list of View Modes Inventory.

## Impressed cards:
* Impressed card - xlarge.
* Impressed card - large.
* Impressed card - medium.
* Impressed card - small.
* Impressed card - xsmall.

## Featured cards:
* Featured card - xlarge.
* Featured card - large.
* Featured card - medium.
* Featured card - small.
* Featured card - xsmall.
    
## Text cards:
* Text card - large.
* Text card - medium.
* Text card - small.

## Overlay cards: 
* Overlay card - xlarge.
* Overlay card - large.
* Overlay card - medium.

## Hero cards:
* Hero card.




## Requirements
* Bootstrap theme or a sub theme.
* Display Suite module.
* Display Suite Extras module.
* Field Group module.
* Smart Trim module.
* Varbase Components.

## Installation


## Configuration



## Maintainers

- Mohammed Razem ([mohammed-j-razem](https://www.drupal.org/u/mohammed-j-razem))
- Rajab Natshah ([RajabNatshah](https://www.drupal.org/u/rajabnatshah))
